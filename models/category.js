var mongoose = require('mongoose');

var categorySchema = new mongoose.Schema({
    nameRUS: {
        type: String,
        required: 'category must have a rus name!'
    },
    nameENG: {
        type: String,
        required: 'category must have an eng name!'
    }
});

var Category =  mongoose.model('Category', categorySchema);

module.exports = Category;