var mongoose = require('mongoose');

var vegasSlideSchema = new mongoose.Schema({
    path: {
        type: String,
        required: 'photo must have a path!'
    }
});

var VegasSlide =  mongoose.model('VegasSlide', vegasSlideSchema);

module.exports = VegasSlide;