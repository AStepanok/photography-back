var mongoose = require('mongoose');

var textSchema = new mongoose.Schema({
    rus: {
        type: String,
        required: 'text must have a rus variant!'
    },
    eng: {
        type: String,
        required: 'text must have an eng variant!'
    }
});

var Text =  mongoose.model('Text', textSchema);

module.exports = Text;