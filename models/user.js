var mongoose = require('mongoose'),
    passwordHash = require('password-hash');


const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        index: { unique: true }
    },
    password: String
});


UserSchema.methods.comparePassword = function comparePassword(password) {
    var res = passwordHash.verify(password, this.password);
    return res;
};

UserSchema.pre('save', function saveHook(next) {
    var user = this;

    if(!user.isModified('password')){
        return next();
    }

    var hashed =  passwordHash.generate(user.password);
    user.password = hashed;

    return next();
});


module.exports = mongoose.model('User', UserSchema);