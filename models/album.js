var mongoose = require('mongoose');

var albumSchema = new mongoose.Schema({
    nameRUS: {
        type: String,
        required: 'album must have a rus name!'
    },
    nameENG: {
        type: String,
        required: 'album must have an eng name!'
    },
    secondLineRUS: {
        type: String,
        required: 'album must have a rus second line!'
    },
    secondLineENG: {
        type: String,
        required: 'album must have an eng second line!'
    },
    category: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
    previewPhoto: {type: mongoose.Schema.Types.ObjectId, ref: 'Photo'},
    photos: [
        {type: mongoose.Schema.Types.ObjectId, ref: 'Photo'}
    ],
    horizontalFirst: {
        type: Boolean,
        default: true
    },
    showInPortfolio: {
        type: Boolean,
        default: false
    },
    priority: {
        type: Number,
        default: 0
    }
});

var Album =  mongoose.model('Album', albumSchema);

module.exports = Album;