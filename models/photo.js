var mongoose = require('mongoose');

var photoSchema = new mongoose.Schema({
    album: {type: mongoose.Schema.Types.ObjectId, ref: 'Album'},
    path: {
        type: String,
        required: 'photo must have a path!'
    },
    path_sm: {
        type: String
    },
    orientation: {
        type: Number,
        required: 'photo must have an orientation'
    }
});

var Photo =  mongoose.model('Photo', photoSchema);

module.exports = Photo;