var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/parshinaDB');
mongoose.Promise = Promise;

module.exports.Text = require('./text');
module.exports.Album = require('./album');
module.exports.Photo = require('./photo');
module.exports.Category = require('./category');
module.exports.User = require('./user');
module.exports.VegasSlide = require('./vegasSlide');
module.exports.PriceBox = require('./pricebox');
module.exports.ReviewPhoto = require('./reviewPhoto');

