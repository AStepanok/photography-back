var mongoose = require('mongoose');

var reviewPhotoSchema = new mongoose.Schema({
    path: {
        type: String,
        required: 'photo must have a path!'
    }
});

var ReviewPhoto =  mongoose.model('ReviewPhoto', reviewPhotoSchema);

module.exports = ReviewPhoto;