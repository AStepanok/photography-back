var mongoose = require('mongoose');

var priceboxSchema = new mongoose.Schema({
    nameRUS: {
        type: String,
        required: 'box must have a rus name!'
    },
    nameENG: {
        type: String,
        required: 'box must have an eng name!'
    },
    priceRUB: {
        type: Number,
        required: 'box must have a price'
    },
    priceUSD: {
        type: Number,
        required: 'box must have a price'
    },
    backgroundPhoto: {
        type: String
    },
    features: []
});

var PriceBox =  mongoose.model('PriceBox', priceboxSchema);

module.exports = PriceBox;