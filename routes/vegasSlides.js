var express = require('express'),
    router = express.Router({mergeParams: true}),
    db = require('../models'),
    multer = require('multer'),
    moment = require('moment'),
    fs = require('fs'),
    auth = require('../controllers/auth');

var tempFilePaths = [];

const storage = multer.diskStorage({
    destination: './files',
    filename(req, file, cb) {
        var path = `${moment().format('YYYYMMDhhmmssSSS')}-${file.originalname}`;
        tempFilePaths.push({
            name: file.originalname,
            path
        });
        cb(null, path);
    },
});

const upload = multer({ storage });

router.get('/', function (req, res) {
    db.VegasSlide.find({})
        .then(slides => res.json(slides))
        .catch(err => console.log(err));
});

router.post('/', auth, upload.any(), function (req, res) {
    let slides = [];
    tempFilePaths.forEach((file) => {
        slides.push({
            path: `https://parshina.rocketwebsites.ru/${file.path}`
        });
    });
    db.VegasSlide.create(slides)
        .then(() => {
            db.VegasSlide.find({})
                .then(slides => {
                    tempFilePaths=[];
                    res.json(slides)
                })
                .catch(err => console.log(err));
        })
});

router.delete('/:photoId',auth,  function (req, res) {
    db.VegasSlide.findById(req.params.photoId)
        .then(slide => {
            let items = slide.path.split('/');
            let name = items[items.length -1];
            fs.unlink(`./files/${name}`, function () {
                slide.remove()
                    .then(() => {
                        db.VegasSlide.find({})
                            .then(slides => res.json(slides))
                            .catch(err => console.log(err));
                    })
            })
        })
});




module.exports = router;