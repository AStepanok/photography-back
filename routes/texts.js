var express = require('express'),
    router = express.Router(),
    db = require('../models'),
    auth = require('../controllers/auth');

router.get('/', function (req, res) {
    db.Text.find()
        .then(texts => res.json(texts))
        .catch(err => console.log(err));
});

router.post('/', auth, function (req, res) {
    db.Text.create(req.body)
        .then(newText => res.json(newText))
        .catch(err => console.log(err));
});

router.put('/:textId', auth, function (req, res) {
    db.Text.findOneAndUpdate({_id: req.params.textId}, req.body, {new: true})
        .then(function (updatedText) {
            res.json(updatedText);
        })
        .catch(function (err) {
            console.log(err);
        });
});


module.exports = router;