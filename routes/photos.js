var express = require('express'),
    router = express.Router({mergeParams: true}),
    db = require('../models'),
    multer = require('multer'),
    moment = require('moment'),
    fs = require('fs'),
    sizeOf = require('image-size'),
    auth = require('../controllers/auth'),
    imageResizer = require('../controllers/imageResizer');

var tempFiles = [];

const storage = imageResizer({
    filename(req, file, cb) {
        var path = `${moment().format('YYYYMMDhhmmssSSS')}-${file.originalname}`;
        tempFilePaths.push({
            name: file.originalname,
            path
        });
        cb(null, path);
    },
    tempFiles: tempFiles
});

const upload = multer({ storage });

router.get('/', function (req, res) {
    db.Photo.find({album: req.params.albumId}).populate('album')
        .then(photos => res.json(photos))
        .catch(err => console.log(err));
});

router.post('/', auth, upload.any(), function (req, res) {
    let photos = [];
    db.Album.findById(req.params.albumId)
        .then((album) => {
            tempFiles.forEach((file) => {
                let ext = file.originalname.split('.')[1];
                let dim = sizeOf(`./files/${file.hashedName}.${ext}`);
                let orientation = dim.width>dim.height ? 0 : 1;
                photos.push({
                    album: album._id,
                    path: `https://parshina.rocketwebsites.ru/${file.hashedName}.${ext}`,
                    path_sm: `https://parshina.rocketwebsites.ru/${file.hashedName}-sm.${ext}`,
                    // path: `http://localhost:8083/${file.hashedName}.${ext}`,
                    // path_sm: `http://localhost:8083/${file.hashedName}-sm.${ext}`,
                    orientation
                });
            });
            db.Photo.create(photos)
                .then((dbPhotos) => {
                    album.photos = [...album.photos, ...dbPhotos.map(photo => photo._id)];
                    if (!album.previewPhoto)
                        album.previewPhoto = dbPhotos[0]._id;
                    album.save()
                        .then(() => {
                            db.Album.findById(album._id)
                                .populate('photos')
                                .populate('category')
                                .populate('previewPhoto')
                                .then(result => {
                                    tempFiles.length = 0;
                                    res.json(result);
                                })
                        })

                })
        })


});

router.delete('/:photoId', auth,  function (req, res) {
    db.Album.findById(req.params.albumId)
        .populate('previewPhoto')
        .populate('photos')
        .populate('category')
        .then(album => {

            album.photos = album.photos.filter(p => p._id != req.params.photoId);

            if (album.previewPhoto._id == req.params.photoId)
                album.previewPhoto = album.photos[0];
            console.log(album);
            album.save()
            .then(() => {
                db.Photo.findById(req.params.photoId)
                    .then(photo => {
                            let items = photo.path.split('/');
                            let name = `./files/${items[items.length -1]}`;
                            let items_sm = photo.path_sm.split('/');
                            let name_sm = `./files/${items_sm[items_sm.length -1]}`;
                            deleteFiles([name, name_sm], function () {
                                photo.remove()
                                    .then(() => {
                                        res.json(album)
                                    })
                                    .catch(function (err) {
                                        console.log(err);
                                    });

                                })
                        })

                });
        });

});

function deleteFiles(files, callback){
    var i = files.length;
    files.forEach(function(filepath){
        fs.unlink(filepath, function (err) {
            i--;
            if (err) {
                callback(err);
                return;
            } else if (i <= 0) {
                callback(null);
            }
        })
    });
}


module.exports = router;