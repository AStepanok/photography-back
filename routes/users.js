var express = require('express'),
    router = express.Router(),
    db = require('../models'),
    jwt = require('jsonwebtoken'),
    passwordHash = require('password-hash');

router.post('/', function (req, res) {

    login(req.body.username, req.body.password, function(err, result){
        if(err){

            res.status(500).json({
                success: 'false',
                error: err
            });
            return;
        }

        if(result){

            res.status(200).json({
                success: 'true',
                data: {tokenID: result, username: req.body.username}
            });
        }else{
            console.log('something went wrong');
            res.status(401).json({
                success: 'false',
                data: result
            });
        }
    });
});

function login(username, password, callback){

    db.User.findOne({ username: username })
        .then((user, err) => {
            if(err){
                callback(err, null);
                return;
            }
            if(!user){
                callback(err, null);
            }else{

                if (passwordHash.verify(password, user.password)) {

                    var authToken = jwt.sign({ username: user.username, _id: user._id}, 'simplesecretstring');
                    callback(null, authToken);
                } else {

                    callback(err, null);
                }
            };
        });
}

module.exports = router;