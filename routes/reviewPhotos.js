var express = require('express'),
    router = express.Router({mergeParams: true}),
    db = require('../models'),
    multer = require('multer'),
    moment = require('moment'),
    fs = require('fs'),
    auth = require('../controllers/auth');

var tempFilePaths = [];

const storage = multer.diskStorage({
    destination: './files',
    filename(req, file, cb) {
        var path = `${moment().format('YYYYMMDhhmmssSSS')}-${file.originalname}`;
        tempFilePaths.push({
            name: file.originalname,
            path
        });
        cb(null, path);
    },
});

const upload = multer({ storage });

router.get('/', function (req, res) {
    db.ReviewPhoto.find({})
        .then(photos => res.json(photos))
        .catch(err => console.log(err));
});

router.post('/', auth, upload.any(), function (req, res) {
    let photos = [];
    tempFilePaths.forEach((file) => {
        photos.push({
            path: `https://parshina.rocketwebsites.ru/${file.path}`
        });
    });
    db.ReviewPhoto.create(photos)
        .then(() => {
            db.ReviewPhoto.find({})
                .then(photos => {
                    tempFilePaths=[];
                    res.json(photos)
                })
                .catch(err => console.log(err));
        })
});

router.delete('/:photoId', auth, function (req, res) {
    db.ReviewPhoto.findById(req.params.photoId)
        .then(photo => {
            let items = photo.path.split('/');
            let name = items[items.length -1];
            fs.unlink(`./files/${name}`, function () {
                photo.remove()
                    .then(() => {
                        db.ReviewPhoto.find({})
                            .then(photos => res.json(photos))
                            .catch(err => console.log(err));
                    })
            })
        })
});




module.exports = router;