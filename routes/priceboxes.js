var express = require('express'),
    router = express.Router({mergeParams: true}),
    db = require('../models'),
    multer = require('multer'),
    moment = require('moment'),
    fs = require('fs'),
    auth = require('../controllers/auth');

var tempFilePaths = [];

const storage = multer.diskStorage({
    destination: './files',
    filename(req, file, cb) {
        var path = `${moment().format('YYYYMMDhhmmssSSS')}-${file.originalname}`;
        tempFilePaths.push({
            name: file.originalname,
            path
        });
        cb(null, path);
    },
});

const upload = multer({ storage });

router.get('/', function (req, res) {
    db.PriceBox.find({})
        .then(boxes => res.json(boxes))
        .catch(err => console.log(err));
});

router.post('/', auth, function (req, res) {
    db.PriceBox.create(req.body)
        .then(newBox => res.json(newBox))
        .catch(err => console.log(err));
});

router.post('/:boxId/photo', auth,  upload.any(), function (req, res) {
    db.PriceBox.findById(req.params.boxId)
        .then(function (box) {
            if (tempFilePaths.length>0) {
                let items = box.backgroundPhoto.split('/');
                let name = items[items.length -1];
                fs.unlink(`./files/${name}`, function () {
                    box.backgroundPhoto = `https://parshina.rocketwebsites.ru/${tempFilePaths[0].path}`;
                    box.save()
                        .then(() => {
                            tempFilePaths = [];
                            res.json(box)
                        })
                        .catch(function (err) {
                            console.log(err);
                        });
                });

            }
            res.json(updatedBox);
        })
        .catch(function (err) {
            console.log(err);
        });
});

router.put('/:boxId', auth, upload.any(), function (req, res) {
    db.PriceBox.findOneAndUpdate({_id: req.params.boxId}, req.body, {new: true})
        .then(function (updatedBox) {
            res.json(updatedBox);
        })
        .catch(function (err) {
            console.log(err);
        });
});

router.delete('/:boxId', auth, function (req, res) {
    db.PriceBox.findById({_id: req.params.boxId})
        .then(box => {
            let items = box.backgroundPhoto.split('/');
            let name = items[items.length -1];
            fs.unlink(`./files/${name}`, function () {
                box.remove()
                    .then(() => {
                        res.json({message:'success'})
                    })
                    .catch(err => console.log(err))
            })
        })
});




module.exports = router;