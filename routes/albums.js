var express = require('express'),
    router = express.Router(),
    db = require('../models'),
    fs = require('fs'),
    auth = require('../controllers/auth');

router.get('/', function (req, res) {
    db.Album.find()
        .populate('category')
        .populate('photos')
        .populate('previewPhoto')
        .then(albums => res.json(albums))
        .catch(err => console.log(err));
});

router.post('/', auth, function (req, res) {
    db.Album.create(req.body)
        .then(newAlbum => res.json(newAlbum))
        .catch(err => console.log(err));
});


router.put('/:albumId', auth, function (req, res) {
    db.Album.findOneAndUpdate({_id: req.params.albumId}, req.body, {new: true})
        .populate('photos')
        .populate('category')
        .populate('previewPhoto')
        .then(function (updatedAlbum) {
            res.json(updatedAlbum);
        })
        .catch(function (err) {
            console.log(err);
        });
});

router.delete('/:albumId', auth, function (req, res) {

    db.Album.findById({_id: req.params.albumId})
        .populate('photos')
        .then(album => {
            let paths = album.photos.reduce((acc,p)  => {
                let items = p.path.split('/');
                let name = `./files/${items[items.length -1]}`;
                let items_sm = p.path_sm.split('/');
                let name_sm = `./files/${items_sm[items_sm.length -1]}`;
                acc.push(name);
                acc.push(name_sm);
                return acc;
            }, []);

            if (paths.length > 0) {
                deleteFiles(paths, function () {

                    let ids = album.photos.map(p => p._id);
                    db.Photo.remove({_id: {$in: ids}})
                        .then(() => {

                            album.remove()
                                .then(() => {
                                    res.json({message: 'success'});
                                })
                                .catch(err => {
                                    console.log(err);
                                })
                        })
                });
            } else {
                album.remove()
                    .then(() => {
                        res.json({message: 'success'});
                    })
                    .catch(err => {
                        console.log(err);
                    })
            }
        })
});

function deleteFiles(files, callback){
    var i = files.length;
    files.forEach(function(filepath){
        fs.unlink(filepath, function (err) {
            i--;
            if (err) {
                callback(err);
                return;
            } else if (i <= 0) {
                callback(null);
            }
        })
    });
}

module.exports = router;