var express = require('express'),
    router = express.Router(),
    db = require('../models'),
    auth = require('../controllers/auth');

router.get('/', function (req, res) {
    db.Category.find()
        .then(categories => res.json(categories))
        .catch(err => console.log(err));
});

router.post('/', auth,  function (req, res) {
    db.Category.create(req.body)
        .then(newCategory => res.json(newCategory))
        .catch(err => console.log(err));
});

router.put('/:catId', auth, function (req, res) {
    db.Category.findOneAndUpdate({_id: req.params.catId}, req.body, {new: true})
        .then(function (updatedCategory) {
            res.json(updatedCategory);
        })
        .catch(function (err) {
            console.log(err);
        });
});

router.delete('/:catId', auth, function (req, res) {
    db.Category.findOneAndRemove({_id: req.params.catId})
        .then(function () {
            res.json({message: 'success'});
        })
        .catch(function (err) {
            console.log(err);
        });
});

module.exports = router;