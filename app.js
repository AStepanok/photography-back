var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    textRoutes = require('./routes/texts'),
    categoryRoutes = require('./routes/categories'),
    albumRoutes = require('./routes/albums'),
    photoRoutes = require('./routes/photos'),
    userRoutes = require('./routes/users'),
    vegasSlideRoutes = require('./routes/vegasSlides'),
    priceBoxRoutes = require('./routes/priceboxes'),
    reviewPhotoRoutes = require('./routes/reviewPhotos'),
    nodemailer = require('nodemailer'),
    auth = require('./controllers/auth'),
    path = require('path');


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(__dirname + '/files/'));
app.use( express.static(`${__dirname}/build`));

app.use('/api/texts/',textRoutes);
app.use('/api/categories/',categoryRoutes);
app.use('/api/albums/',albumRoutes);
app.use('/api/albums/:albumId/photos',photoRoutes);
app.use('/api/users/',userRoutes);
app.use('/api/vegasslides/',vegasSlideRoutes);
app.use('/api/priceboxes/',priceBoxRoutes);
app.use('/api/reviewphotos/',reviewPhotoRoutes);



var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'email',
        pass: 'password'
    }
});

app.get('*', (req, res)=>{
    res.sendFile(path.join(__dirname, '/build/index.html'));
});

app.post('/api/email', auth, function (req, res) {
    var properitesNames = {
        'fio': 'ФИО',
        'phone': 'Номер телефона',
        'comments': 'Комментарий'
    };

    var mailOptions = {
        from: 'anton.stepanok@gmail.com',
        // to: 'parshinadaria14@gmail.com',,
        to: 'anton.stepanok@gmail.com',
        subject: 'Новое обращение с твоего сайта'
    };
    var bodyStr = "";
    for (var key in req.body) {
        bodyStr += '<h3>' + properitesNames[key] + ":</h3> <p>" + req.body[key] + '</p>';
    }
    mailOptions.html = '<h1>Новое обращение с твоего сайта!</h1>' + bodyStr;
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
            res.json({"error" : "Something went wrong", "status" : 500});
        } else {
            console.log('Email sent: ' + info.response);
            res.json({"success" : "Email sent succesfully", "status" : 200});
        }
    });

});

app.listen(8083, function () {
    console.log('Server is running on port 8083');
});