var jwt = require('jsonwebtoken');

module.exports = function isLoggedIn(req, res, next) {
    if (!req.headers.authorization) {
        console.log('Unauthorized');
        return res.status(401).end();
    }

    const token = req.headers.authorization.split(' ')[1];

    return jwt.verify(token, 'simplesecretstring', (err, decoded) => {

        if (err) {
            console.log('Unauthorized');
            return res.status(401).end();
        }

        req.userData = {};
        req.userData.tokenID  = token;
        req.userData.userid = decoded._id;
        req.userData.username = decoded.username;

        return next();


    });
};