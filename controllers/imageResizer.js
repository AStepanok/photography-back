var fs = require('fs'),
    sharp = require('sharp'),
    moment = require('moment');

function getDestination (req, file, cb) {
    cb(null, './files/')
}

function MyCustomStorage (opts) {
    this.getDestination = (opts.destination || getDestination);
    this.tempFiles = opts.tempFiles
}

MyCustomStorage.prototype._handleFile = function _handleFile (req, file, cb) {
    var tempFiles = this.tempFiles;
    this.getDestination(req, file, function (err, path) {
        if (err) return cb(err);


        let filename = moment().format('YYYYMMDhhmmssSSS');
        let resizer = sharp().resize(100, 100).max();

        let outStream_sm = fs.createWriteStream(path+filename
            +'-sm.'+file.originalname.split('.')[1]);
        let outStream_lg = fs.createWriteStream(path+filename + '.' + file.originalname.split('.')[1]);

        tempFiles.push({
            originalname: file.originalname,
            hashedName: filename
        });

        file.stream.pipe(resizer).pipe(outStream_sm);
        file.stream.pipe(outStream_lg);
        outStream_lg.on('error', cb);
        outStream_lg.on('finish', function () {
            cb(null, {
                path: path,
                size: outStream_lg.bytesWritten
            })
        })
    })
}

MyCustomStorage.prototype._removeFile = function _removeFile (req, file, cb) {
    fs.unlink(file.path, cb)
}

module.exports = function (opts) {
    return new MyCustomStorage(opts)
}